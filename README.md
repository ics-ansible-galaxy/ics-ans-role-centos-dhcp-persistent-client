# ics-ans-role-centos-dhcp-persistent-client

Ansible role to add PERSISTENT_DHCLIENT to redhat (and centos) ifcfg-* files

## Example Playbook

```yaml
- hosts: all
  roles:
    - role: ics-ans-role-centos-dhcp-persistent-client
```

## License

BSD 2-clause
