import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('debians')


def test_debian(host):
    assert "PERSISTENT_DHCLIENT=yes" not in host.file("/etc/sysconfig/network-scripts/ifcfg-am1").content_string
    assert "PERSISTENT_DHCLIENT=yes" not in host.file("/etc/sysconfig/network-scripts/ifcfg-ith1").content_string
    assert "PERSISTENT_DHCLIENT=yes" not in host.file("/etc/sysconfig/network-scripts/ifcfg-lo").content_string
