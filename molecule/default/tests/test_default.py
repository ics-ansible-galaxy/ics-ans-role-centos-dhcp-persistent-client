import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('centoses')


def test_centos(host):
    assert "PERSISTENT_DHCLIENT=yes" in host.file("/etc/sysconfig/network-scripts/ifcfg-am1").content_string
    assert "PERSISTENT_DHCLIENT=yes" not in host.file("/etc/sysconfig/network-scripts/ifcfg-ith1").content_string
    assert "PERSISTENT_DHCLIENT=yes" not in host.file("/etc/sysconfig/network-scripts/ifcfg-lo").content_string
