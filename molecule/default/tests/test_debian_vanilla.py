import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('debians_vanilla')


def test_debian_vanilla(host):
    assert not host.file("/etc/sysconfig/network-scripts/ifcfg-am1").exists
    assert not host.file("/etc/sysconfig/network-scripts/ifcfg-ith1").exists
    assert not host.file("/etc/sysconfig/network-scripts/ifcfg-lo").exists
